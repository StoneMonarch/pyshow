import datetime
import libtorrent
import logging
import os
import re
import signal
import sqlite3
import sys
import time

from sqlite3 import Error

# Details
program_name = 'PyShow'
program_version = '0.0.1'

# Directory Locations
database_location = 'database/'
tvshows_location = '/home/plex/media/TV Shows/'
movie_location = '/home/plex/media/Movies/'
logging_location = 'log/'

# Database Connections
database_connection = None
cursor = None
database_name = 'database.sqlite'

# Defaults
codecs = ['x264', '264', 'x265', '265']
resolutions = [4320, 2160, 1440, 1080]

# Trakt.tv
client_id = '716f2218d28cd578b9f7817c2cab46e91b612d41a1f58be5a9f84931f2d4bb89'
client_secret = '865e6c184612551b816aea2a74f0c1c5781c8182cd86bccb56bdc2320e554669'
device_code = None
token = None

# Logging
formatting = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
log_file_name = datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S" + '.log')
log_file = open('lol.txt', 'w+')
log_file.close()

sql_tv_show_table = '''create table tv_shows 
(id integer not null constraint tv_shows_pk primary key,
show_name text not null,
season int not null,
episode int not null,
episode_name text not null,
trakt_id integer,
downloaded integer not null
);'''
"""
PyShow is a program to manage a growing media
library for Plex Media Server. PyShow uses
the trakt.tv API endpoint to query episodes 
on a users calendar, and them proceeds to 
try to find the TV Show on The Pirate Bay,
torrent it, rename it, and finally move it 
into the correct directory.
"""


class Killer:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit)
        signal.signal(signal.SIGTERM, self.exit)

    def exit(self, signum, frame):
        self.kill_now = True


def index_current_shows():
    shows = os.listdir(tvshows_location)
    for show in range(len(shows)):
        seasons = os.listdir(tvshows_location + shows[show])

        # Show Name
        show_name = shows[show]
        logger.debug('Entering ' + show_name)

        for season in range(len(seasons)):
            episodes = os.listdir(tvshows_location + shows[show] + '/' + seasons[season])

            # Season Number
            season_number = int(re.split(' ', seasons[season])[1])
            logger.debug('Entering Season' + str(season_number))

            for episode in range(len(episodes)):
                # Episode number as INT
                episode_number = re.split(' - ', episodes[episode])[1]
                episode_number = int(episode_number[4] + episode_number[5])

                # Episode Name
                episode_name = re.split(' - ', episodes[episode])[2]
                episode_name = episode_name[:-4]
                print(show_name + ' - S' + "{0:0=2d}".format(season_number) + 'E' + "{0:0=2d}".format(
                    episode_number) + ' - ' + episode_name)
                cursor.execute(
                    "INSERT INTO tv_shows(show_name, season, episode, episode_name, downloaded) VALUES (?,?,?,?,?) ",
                    (show_name, season_number, episode_number, episode_name, 1))
                database_connection.commit()


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return conn


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        cursor.execute(create_table_sql)
    except Error as e:
        print(e)


def database_show_insert(name, season, episod, episode_name):
    pass


def sorted_ls(path):
    mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
    return list(sorted(os.listdir(path), key=mtime))


def magnet2torrent(magnet_link, output_path):
    session = libtorrent.session()
    parameters = {
        'save_path': os.path.abspath(output_path),
        'paused': False,
        'auto_managed': False,
        'upload_mode': True,
    }
    torrent = libtorrent.add_magnet_uri(session, magnet_link, parameters)
    dots = 0
    while not torrent.has_metadata():
        dots += 1
        sys.stdout.write('.')
        sys.stdout.flush()
        time.sleep(1)
    if dots: sys.stdout.write('\n')
    session.pause()
    torrent_info = torrent.get_torrent_info()
    output_file = open(output_path + torrent_info.name() + '.torrent', 'wb')
    output_file.write(libtorrent.bencode(
        libtorrent.create_torrent(torrent_info).generate()))
    output_file.close()
    session.remove_torrent(torrent)


try:
    os.mkdir(logging_location)
except FileExistsError:
    print('')
    del_list = sorted_ls(logging_location)[0:(len(sorted_ls(logging_location)) - 3)]

    for dfile in del_list:
        os.remove(logging_location + dfile)

try:
    # Create target Directory
    os.mkdir(database_location)
except FileExistsError:
    pass

logger = logging.getLogger(program_name)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(str(log_file))
fh.setLevel(logging.DEBUG)
sh = logging.StreamHandler()
sh.setLevel(logging.WARNING)
formatter = logging.Formatter(formatting)
fh.setFormatter(formatter)
sh.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(sh)

logger.debug('Logger Set Up')
print("Logger init: Complete")

if not os.path.isfile(database_location + database_name):
    database_connection = create_connection(database_location + database_name)
    cursor = database_connection.cursor()
    if database_connection is not None:
        # create projects table
        create_table(database_connection, sql_tv_show_table)
    else:
        logger.critical('Unable to reach database! Exiting.')
        exit(0)

# TODO: Move into IF when done testing
index_current_shows()
