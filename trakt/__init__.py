try:
    from trakt.core import *
except ImportError:
    pass

version_info = (2, 9, 0)
__version__ = '.'.join([str(i) for i in version_info])
